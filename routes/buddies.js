const router = require("express").Router();
const {
  getBuddiesInfo,
  getSepecificBuddyInfo,
  addBuddy,
  editBuddy,
  deleteButton,
} = require("../controller/buddies.js");
const {routerConstants} = require('../constants/constants.js');

router.get(routerConstants.BUDDIES_ROUTE, getBuddiesInfo);
router.get(routerConstants.SPECIFIC_BUDDY_ROUTE, getSepecificBuddyInfo);
router.post(routerConstants.BUDDY_ROUTE, addBuddy);
router.put(routerConstants.BUDDY_ROUTE, editBuddy);
router.delete(routerConstants.SPECIFIC_BUDDY_ROUTE, deleteButton);

module.exports = router;
