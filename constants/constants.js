const responseConstants = {
  SUCCESS: "Success",
  NOT_VALID: "Not Valid",
  ALREADY_EXISTS: "Already Exists",
  NOT_EXISTS: "Not Exists",
  NOT_SUCCESS: "Not Success",
  INTERNAL_ERROR: "Internal Error",
};

const generalConstants = {
  GET_BUDDIES: "Get buddies",
  GET_SPECIFIC_BUDDIES: "Get specific buddies",
  ADD_BUDDY: "add buddy",
  EDIT_BUDDY: "edit buddy",
  DELETE_BUDDY: "Delete buddy",
  START: "Start",
  END: "End",
  UNDEFINED: "undefined",
  FILE_PATH: "./assets/cdw_ace23_buddies.json",
  FILE_CREATED: "file created",
};

const serviceLayerConstants = {
  START: "Start",
  END: "End",
  CREATE: "Create",
  UPDATE: "Update",
  READ: "Read",
  DELETE: "Delete",
  ALREADY_EXISTS: "already exists",
  SUCCESS: "success",
  DATA_DELETED: "data deleted successfully",
  DATA_DOES_NOT_EXITS: "data not exits",
  UPDATE_SUCCESS: "update success",
  NOT_EXISTS: "not exists",
};

const regexConstants = {
  ID: /\d*/,
  REAL_NAME: /[\w]{1,30}/,
  NICK_NAME: /[\w]{1,30}/,
  DOB: /\d{1,2}\/\d{1,2}\/\d{2,4}/,
  HOBBIES: /[\w]{1,30}/,
};

const validateConstants = {
  VALIDATE_DATA: ["employeeId", "realName", "nickName", "dob", "hobbies"],
};

const routerConstants = {
  BUDDIES_ROUTE:"/buddies",
  SPECIFIC_BUDDY_ROUTE:"/buddy/:id",
  BUDDY_ROUTE:"/buddy",
  GENERAL_ROUTE:"/api/v1",
}


module.exports = {
  responseConstants,
  generalConstants,
  serviceLayerConstants,
  regexConstants,
  validateConstants,
  routerConstants
};