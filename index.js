const express = require("express");
const fs = require("fs");
const { createFile } = require("./helper/createFile.js");
const { generalConstants,routerConstants } = require("./constants/constants.js");
const swaggerUI = require('swagger-ui-express');
const swaggerSpec = require('./helper/swagger');
const cors=require('cors');
const app = express();
require("dotenv").config();
// route import
const buddies = require("./routes/buddies.js");

app.use(express.json());
app.use(cors())

app.use('/buddies-docs', swaggerUI.serve, swaggerUI.setup(swaggerSpec));

app.listen(process.env.PORT || 5001);

app.use(routerConstants.GENERAL_ROUTE, buddies);

fs.stat(generalConstants.FILE_PATH, async (error, status) => {
  if (error) {
     await createFile(generalConstants.FILE_PATH);

  }
});
