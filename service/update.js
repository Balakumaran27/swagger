const fs = require("fs");
const { read } = require("./read");
const {
  serviceLayerConstants,
} = require("../constants/constants.js");
/**
 * This function is used to update the database
 * @param {*} filePath This is the location of the db
 * @param {*} buddyData  This is the data to be inserted into the db
 * @returns returns a promise
 */
const update = (filePath, buddyData) => {
  return new Promise(async (resolve, reject) => {
    let buddiesdata = await read(filePath);

    let alreadyExists =
      (await read(filePath, buddyData?.employeeId)).length !== 0 ? 1 : 0;
    if (alreadyExists) {
      buddiesdata.map((buddy, index) => {
        if (buddy.employeeId === buddyData.employeeId) {
          buddiesdata[index] = buddyData;
        }
      });
      fs.writeFile(filePath, JSON.stringify(buddiesdata), (err) => {
        if (err) {
          
          reject(err);
        }

        resolve(serviceLayerConstants.UPDATE_SUCCESS);
      });
    } else {
      resolve(serviceLayerConstants.NOT_EXISTS);
    }
  });
};

module.exports = {
  update,
};
