const { generalConstants } = require("../constants/constants.js");
const fs = require("fs");
/**
 * This function is used to create the file at the initialization
 * @param {*} filePath
 * @returns
 */
const createFile = (filePath) => {
  return new Promise((resolve, reject) => {
    fs.writeFile(filePath, JSON.stringify([]), (err) => {
      if (err) {
        resolve(err);
      } else {
        resolve(generalConstants.FILE_CREATED);
      }
    });
  });
};

module.exports = { createFile };
