const fs = require("fs");
const { read } = require("./read");
const { serviceLayerConstants } = require("../constants/constants.js");
/**
 * This function is used to delete the buddy from the db
 * @param {*} filePath This is the filepath for the db
 * @param {*} buddyID This is the buddy id
 * @returns returns a promise
 */
const deleteBuddy = (filePath, buddyID) => {
  return new Promise(async (resolve, reject) => {
    let alreadyExists = (await read(filePath, buddyID)).length !== 0 ? 1 : 0;
    if (alreadyExists) {
      let buddiesdata = (await read(filePath))?.filter(
        (buddy) => buddy.employeeId !== buddyID
      );
      fs.writeFile(filePath, JSON.stringify(buddiesdata), (err) => {
        if (err) {
         
          reject(err);
        }

        resolve(serviceLayerConstants.DATA_DELETED);
      });
    } else {
      resolve(serviceLayerConstants.DATA_DOES_NOT_EXITS);
    }
  });
};

module.exports = {
  deleteBuddy,
};
