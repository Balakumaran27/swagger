const swaggerJSDoc = require("swagger-jsdoc");

const swaggerDefinition = {
openapi: '3.0.0',
info: {
title: 'Buddies Api',
version: '1.0.0',
description: 'This is a proof of concept for swagger docs',
},
servers:[
      {
        url:"http://localhost:5500"
      }
],
};

const options = {
swaggerDefinition,
apis: ['./controller/buddies.js'], // Path to the API routes in your Node.js application
};

const swaggerSpec = swaggerJSDoc(options);
module.exports = swaggerSpec