const { regexConstants } = require("../constants/constants.js");
/**
 * This function is used to validate the id
 * @param {*} id - this is the employee id
 * @returns - this will be true is the  id is a valid digit
 */
const validateId = (id) => {
  console.log(id,"dssd");
  return id.match(regexConstants.ID)[0] === id;
};

module.exports = { validateId };
