const fs = require("fs");
const {
  generalConstants
} = require("../constants/constants.js");
/**
 * This is used to read the data from the db
 * @param {*} filePath This is the location of the db
 * @param {*} buddyID  This is the buddy id
 * @returns returns a promise
 */

const read = (filePath, buddyID) => {
  return new Promise((resolve, reject) =>
    fs.readFile(filePath, "utf-8", (err, data) => {
      if (err) {
        reject(err);
      }
      
      if (!(typeof buddyID === generalConstants.UNDEFINED)) {
        let buddyData = JSON.parse(data).filter(
          (buddy) => buddy.employeeId === buddyID
        );

        resolve(buddyData);
      } else {
        resolve(JSON.parse(data));
      }
    })
  );
};

module.exports = {
  read,
};
