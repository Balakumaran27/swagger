/**
 * This function is used to handle the response as object
 * @param {*} status 
 * @param {*} data 
 * @param {*} statusCode 
 * @returns 
 */
const responseHandler = (status, data, statusCode) => {
  return { status: status, data: data, statusCode: statusCode };
};

module.exports = {
  responseHandler,
};
