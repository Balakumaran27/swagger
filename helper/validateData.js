const { validateId } = require("./validateId");
const {
  regexConstants,
  validateConstants,
} = require("../constants/constants.js");
/**
 * This function is used to validate the data
 * @param {*} data - this is the data that is sent for validation
 * @returns returns true if the data is valid or false if its not
 */
const validateData = (data) => {
  if (!validateId(data?.employeeId)) {
    return false;
  }
  let regexValidations = {
    realName: regexConstants.REAL_NAME,
    nickName: regexConstants.NICK_NAME,
    dob: regexConstants.DOB,
    hobbies: regexConstants.HOBBIES,
  };
  let mustHaveKeys = validateConstants.VALIDATE_DATA;
  let count = 0;
  for (let key in data) {
    if (regexValidations[key]) {
      let matchedData = data[key].match(regexValidations[key]);
      if (
        data[key] === "" ||
        !matchedData?.[0] ||
        !matchedData[0] === data[key]
      ) {
        return false;
      }
    }
    if (key !== validateConstants.VALIDATE_DATA[0] && !regexValidations[key]) {
      return false;
    }
    if (mustHaveKeys.includes(key)) {
      count += 1;
    }
  }
  if (mustHaveKeys.length === count) {
    return true;
  } else {
    return false;
  }
};

module.exports = { validateData };
