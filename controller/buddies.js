const { read } = require("../service/read.js");
const { create } = require("../service/create.js");
const { update } = require("../service/update.js");
const { deleteBuddy } = require("../service/delete.js");
const { validateId } = require("../helper/validateId.js");
const { validateData } = require("../helper/validateData.js");
const { responseHandler } = require("../helper/responseHandler.js");
const {
  responseConstants,
  generalConstants,
  serviceLayerConstants,
} = require("../constants/constants.js");

/**
 * @swagger
 * tags:
 *  - name: buddies
 *    description: Everything about buddies
 *  - name: buddie
 *    description: Everything about buddies
 *
 *
 */

/* components:
 *    schemas:
 *       Buddy:
 *          properties:
 *            newsId:
 *              type: string
 *            realName:
 *              type: string
 *            nickName:
 *              type: string
 *            dob:
 *              type: string
 *            hobbies:
 *              type: string
 *       ResponseObject:
 *          properties:
 *            status:
 *              type: string
 *            data:
 *              type: object
 *            statusCode:
 *              type: string
 */

/**
 * @swagger
 * /api/v1/buddies:
 *   get:
 *     tags:
 *        - buddies
 *     description: Retrieve a list of buddies
 *     responses:
 *       '200':
 *         description: OK
 *         content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/ResponseObject'
 *
 *
 */
const getBuddiesInfo = async (req, res) => {
  let data = await read(generalConstants.FILE_PATH);

  res.json(responseHandler(responseConstants.SUCCESS, data, 200));
};

/**
 * @swagger
 * /api/v1/buddy/{id}:
 *   get:
 *     tags:
 *        - buddie
 *     summary: Get a buddy by ID
 *     parameters:
 *        - in: path
 *          name: id
 *          schema:
 *              type: string
 *          required: true
 *          description: Id of the buddy is used to get their specific detail
 *     responses:
 *       '200':
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/ResponseObject'
 *          description: found
 *       '404':
 *         description: Not found
 *         content:
 *           application/json:
 *              schema:
 *                allOf:
 *                    - $ref: '#/components/schemas/ResponseObject'
 *                    -  properties:
 *                        data:
 *                          type: string
 *       '400':
 *         description: Not valid
 *         content:
 *           application/json:
 *              schema:
 *                $ref: '#/components/schemas/ResponseObject'
 */
const getSepecificBuddyInfo = async (req, res) => {
  let id = req.params?.id;
  console.log(id);
  if (validateId(id)) {
    let data = await read(generalConstants.FILE_PATH, id);
    if (data.length === 0) {
      res.json(responseHandler(responseConstants.NOT_EXISTS, "data", 404));
    } else {
      res.json(responseHandler(responseConstants.SUCCESS, data, 200));
    }
  } else {
    res.json(responseHandler(responseConstants.NOT_VALID, [], 400));
  }
};
/**
 * @swagger
 * /api/v1/buddy:
 *  post:
 *     tags:
 *        - buddies
 *     summary: create a buddy
 *     requestBody:
 *        description: Optional description in *Markdown*
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Buddy'
 *     responses:
 *       '201':
 *         description: OK
 *         content:
 *           application/json:
 *              schema:
 *                $ref: '#/components/schemas/ResponseObject'
 *       '403':
 *         description: Already Exists
 *         content:
 *           application/json:
 *              schema:
 *                $ref: '#/components/schemas/ResponseObject'
 *       '404':
 *         description: Not found
 *         content:
 *           application/json:
 *              schema:
 *                $ref: '#/components/schemas/ResponseObject'
 *       '400':
 *         description: Not valid
 *         content:
 *           application/json:
 *              schema:
 *                $ref: '#/components/schemas/ResponseObject'
 *       '422':
 *         description: Not success
 *         content:
 *           application/json:
 *              schema:
 *                $ref: '#/components/schemas/ResponseObject'
 */

const addBuddy = async (req, res) => {
  let buddyData = req?.body;
  if (validateData(buddyData)) {
    let message = await create(generalConstants.FILE_PATH, buddyData);
    if (message === serviceLayerConstants.SUCCESS) {
      res.json(responseHandler(responseConstants.SUCCESS, [buddyData], 201));
    } else if (message === serviceLayerConstants.ALREADY_EXISTS) {
      res.json(responseHandler(responseConstants.ALREADY_EXISTS, [], 403));
    } else {
      res.json(responseHandler(responseConstants.NOT_SUCCESS, [], 422));
    }
  } else {
    res.json(responseHandler(responseConstants.NOT_VALID, [], 400));
  }
};
/**
 * @swagger
 * /api/v1/buddy:
 *  put:
 *     tags:
 *        - buddies
 *     summary: create a buddy
 *     requestBody:
 *        description: Optional description in *Markdown*
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Buddy'
 *     responses:
 *       '201':
 *         description: created successfully
 *         content:
 *           application/json:
 *              schema:
 *                $ref: '#/components/schemas/ResponseObject'
 *       '404':
 *         description: Not found
 *         content:
 *           application/json:
 *              schema:
 *                $ref: '#/components/schemas/ResponseObject'
 *       '400':
 *         description: Not valid
 *         content:
 *           application/json:
 *              schema:
 *                $ref: '#/components/schemas/ResponseObject'
 */
const editBuddy = async (req, res) => {
  let buddyData = req?.body;
  if (validateData(buddyData)) {
    let message = await update(generalConstants.FILE_PATH, buddyData);
    if (message === serviceLayerConstants.UPDATE_SUCCESS) {
      res.json(responseHandler(responseConstants.SUCCESS, [buddyData], 201));
    } else {
      res.json(responseHandler(responseConstants.NOT_EXISTS, [], 404));
    }
  } else {
    res.json(responseHandler(responseConstants.NOT_VALID, [], 400));
  }
};
/**
 * @swagger
 * /api/v1/buddy/{id}:
 *   delete:
 *     tags:
 *        - buddies
 *     summary: Delete a buddy by ID
 *     parameters:
 *        - in: path
 *          name: id
 *          schema:
 *              type: string
 *          required: true
 *          description: Id of the buddy is used to delete their specific detail
 *     responses:
 *       '200':
 *         description: OK
 *         content:
 *           application/json:
 *              schema:
 *                $ref: '#/components/schemas/ResponseObject'
 *       '404':
 *         description: Not found
 *         content:
 *           application/json:
 *              schema:
 *                $ref: '#/components/schemas/ResponseObject'
 *       '400':
 *         description: Not valid
 *         content:
 *           application/json:
 *              schema:
 *                $ref: '#/components/schemas/ResponseObject'
 */
const deleteButton = async (req, res) => {
  let id = req.params?.id;
  if (validateId(id)) {
    let message = await deleteBuddy(generalConstants.FILE_PATH, id);
    if (message === serviceLayerConstants.DATA_DELETED) {
      res.json(responseHandler(responseConstants.SUCCESS, [], 200));
    } else {
      res.json(responseHandler(responseConstants.NOT_EXISTS, [], 404));
    }
  } else {
    res.json(responseHandler(responseConstants.NOT_VALID, [], 400));
  }
};

module.exports = {
  getBuddiesInfo,
  getSepecificBuddyInfo,
  addBuddy,
  editBuddy,
  deleteButton,
};
