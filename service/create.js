const fs = require("fs");
const { read } = require("./read");
const {
  serviceLayerConstants,
} = require("../constants/constants.js");
/**
 * This is used to do the create the buddy in the biddies database
 * @param {*} filePath  This is the filepath for the db
 * @param {*} buddyData  This is the data to be inserted into the db
 * @returns returns a promise
 */
const create = (filePath, buddyData) => {
  return new Promise(async (resolve, reject) => {
    let buddiesdata = await read(filePath);

    let alreadyExists =
      (await read(filePath, buddyData?.employeeId)).length !== 0 ? 1 : 0;
    
    if (alreadyExists) {
      resolve(serviceLayerConstants.ALREADY_EXISTS);
    } else {
      buddiesdata.push(buddyData);
      fs.writeFile(filePath, JSON.stringify(buddiesdata), (err) => {
        if (err) {
          
          reject(err);
        }
        
        resolve(serviceLayerConstants.SUCCESS);
      });
    }
  });
};

module.exports = {
  create,
};
